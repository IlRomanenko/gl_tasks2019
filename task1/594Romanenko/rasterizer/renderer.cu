#include <common/common_functions.cuh>
#include <common/CudaError.h>

#include <common/CudaForwardDefines.h>

__device__ void _get_projection(const Triangle triangle, const VirtualScreen screen, glm::fvec2 *projection) {

    // простой вариант - https://habr.com/ru/post/248611/
    float r = -1 / screen.observer;
    projection[0] = {triangle.a.x / (1 + triangle.a.z * r), triangle.a.y / (1 + triangle.a.z * r)};
    projection[1] = {triangle.b.x / (1 + triangle.b.z * r), triangle.b.y / (1 + triangle.b.z * r)};
    projection[2] = {triangle.c.x / (1 + triangle.c.z * r), triangle.c.y / (1 + triangle.c.z * r)};

    for (int i = 0; i < 3; i++) {
        projection[i].x *= screen.width_scale;
        projection[i].y *= screen.height_scale;
    }
}

__device__ glm::fvec3 _get_original_point(const Triangle triangle, const glm::fvec3 coefs,
                                          const VirtualScreen screen, glm::fvec3 &orig_coefs) {

    // получаем правильный z-индекс и настоящую точку - https://habr.com/ru/post/249467/
    float r = -1 / screen.observer;
    glm::fvec3 z_coords = {triangle.a.z, triangle.b.z, triangle.c.z};
    orig_coefs = coefs / (z_coords * r + glm::fvec3(1, 1, 1));
    orig_coefs /= (orig_coefs[0] + orig_coefs[1] + orig_coefs[2]);
    return orig_coefs[0] * triangle.a + orig_coefs[1] * triangle.b + orig_coefs[2] * triangle.c;
}

__global__ void _clear_depth_buffer(int *depth_buffer, const VirtualScreen screen) {
    int width = screen.half_width * 2;
    glm::ivec2 pixelVector = {blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y};
    int pixelId = pixelVector.y * width + pixelVector.x;

    if (pixelId < 0 || pixelId >= screen.half_height * screen.half_width * 4) {
        return;
    }

    depth_buffer[pixelId] = 1000 * 1000 * 1000;
}

__device__ void _rasterize_triangle(const Triangle triangle, glm::fvec2 cur_point,
                                    int *depth_buffer, glm::u8vec4 *dst_image, int pixelId,
                                    const VirtualScreen screen) {
    glm::fvec2 projection[3];
    _get_projection(triangle, screen, projection);

    glm::fvec2 directions[2] = {projection[1] - projection[0], projection[2] - projection[0]};

    glm::fvec3 coefs = glm::cross(
            glm::fvec3(directions[0].x, directions[1].x, (projection[0] - cur_point).x),
            glm::fvec3(directions[0].y, directions[1].y, (projection[0] - cur_point).y));

    if (abs(coefs[2]) < 1e-7) {
        return;
    }

    coefs /= coefs[2];
    coefs = {1 - coefs[0] - coefs[1], coefs[0], coefs[1]}; // alpha, beta, gamma

    if (coefs[0] < 0 || coefs[1] < 0 || coefs[2] < 0) {
        return;
    }

    glm::fvec3 orig_coefs;
    glm::fvec3 orig_point = _get_original_point(triangle, coefs, screen, orig_coefs);

    glm::fvec4 result_color = (orig_coefs[0] * triangle.color_a +
                               orig_coefs[1] * triangle.color_b +
                               orig_coefs[2] * triangle.color_c) * glm::fvec4(255);

    int depth = (int) ((orig_point.z - screen.observer) * 1e5); // Согласованно с "быстрым" вариантом

    if (depth_buffer[pixelId] > depth) {
        depth_buffer[pixelId] = depth;
        dst_image[pixelId] = result_color;
    }
}

__global__ void _render_triangles(
        const Triangle *triangles,
        size_t count,
        int *depth_buffer,
        glm::u8vec4 *dst_image,
        const VirtualScreen screen) {

    int width = screen.half_width * 2;

    glm::ivec2 pixelVector = {blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y};
    int pixelId = pixelVector.y * width + pixelVector.x;

    if (pixelId < 0 || pixelId >= screen.half_height * screen.half_width * 4) {
        return;
    }

    for (size_t i = 0; i < count; i++) {
        glm::fvec2 cur_point = {pixelVector.x - screen.half_width, pixelVector.y - screen.half_height};
        _rasterize_triangle(triangles[i], cur_point, depth_buffer, dst_image, pixelId, screen);
    }
};

void render_triangles(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                      const VirtualScreen screen) {

    glm::uvec3 blockDim = {1, 1, 1};
    dim3 blkDim = {blockDim.x, blockDim.y, blockDim.z};
    glm::uvec3 screenDim = {(uint) screen.half_width * 2, (uint) screen.half_height * 2, 1};
    glm::uvec3 gridDim = glm::uvec3(glm::ivec3(screenDim + blockDim) - 1) / blockDim;
    dim3 grdDim = {gridDim.x, gridDim.y, gridDim.z};

    _render_triangles << < grdDim, blkDim >> > (triangles, count, depth_buffer, dst_image, screen);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
}

__device__ void _fill_depth_color_by_pixel(const Triangle triangle, const glm::fvec2 projection[3],
                                           const glm::fvec2 cur_point, const int pixelId,
                                           int *depth_buffer, glm::u8vec4 *dst_image, const bool isDepthMode,
                                           const VirtualScreen screen) {
    glm::fvec2 directions[2] = {projection[1] - projection[0], projection[2] - projection[0]};

    glm::fvec3 coefs = glm::cross(
            glm::fvec3(directions[0].x, directions[1].x, (projection[0] - cur_point).x),
            glm::fvec3(directions[0].y, directions[1].y, (projection[0] - cur_point).y));

    if (abs(coefs[2]) < 1e-7) {
        return;
    }

    coefs /= coefs[2];
    coefs = {1 - coefs[0] - coefs[1], coefs[0], coefs[1]}; // alpha, beta, gamma


    if (coefs[0] < 0 || coefs[1] < 0 || coefs[2] < 0) {
        return;
    }
    glm::fvec3 orig_coefs;
    glm::fvec3 orig_point = _get_original_point(triangle, coefs, screen, orig_coefs);
    int depth = (int) ((orig_point.z - screen.observer) * 1e5); // Грязные хаки
    glm::fvec4 result_color = (orig_coefs[0] * triangle.color_a +
                               orig_coefs[1] * triangle.color_b +
                               orig_coefs[2] * triangle.color_c) * glm::fvec4(255);

    if (isDepthMode) {
        while (depth_buffer[pixelId] > depth) {
            atomicMin(&depth_buffer[pixelId], depth);
        }
    } else {
        if (depth_buffer[pixelId] == depth) {
            dst_image[pixelId] = result_color;
        }
    }
}

__global__ void
_fast_render_triangles(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                       const bool isDepthMode, const VirtualScreen screen) {
    int triangleId = blockIdx.x;

    if (triangleId >= count) {
        return;
    }

    const Triangle triangle = triangles[triangleId];

    glm::fvec2 projection[3];
    _get_projection(triangle, screen, projection);

    int xmin, xmax, ymin, ymax;
    xmin = max(-screen.half_width, (int) (min(min(projection[0].x, projection[1].x), projection[2].x) - 1));
    xmax = min(screen.half_width, (int) (max(max(projection[0].x, projection[1].x), projection[2].x)) + 1);

    ymin = max(-screen.half_height, (int) (min(min(projection[0].y, projection[1].y), projection[2].y) - 1));
    ymax = min(screen.half_height, (int) (max(max(projection[0].y, projection[1].y), projection[2].y) + 1));

    uint pixel_width = (uint) (xmax - xmin);
    uint pixel_height = (uint) (ymax - ymin);

    glm::ivec2 curBlockDim = {blockDim.x, blockDim.y};
    glm::ivec2 pixels_per_thread = (glm::ivec2(pixel_width, pixel_height) + curBlockDim - 1) / curBlockDim;

    xmin = xmin + pixels_per_thread.x * threadIdx.x;
    xmax = min(xmax, xmin + pixels_per_thread.x);

    ymin = ymin + pixels_per_thread.y * threadIdx.y;
    ymax = min(ymax, ymin + pixels_per_thread.y);

    int width = 2 * screen.half_width;
    for (int x = xmin; x < xmax; x++) {
        for (int y = ymin; y < ymax; y++) {
            int pixelId = (y + screen.half_height) * width + (x + screen.half_width);
            _fill_depth_color_by_pixel(triangle, projection, {x, y}, pixelId, depth_buffer, dst_image, isDepthMode,
                                       screen);
        }
    }
}

void clear_depth_buffer(int *depth_buffer, const VirtualScreen screen) {
    dim3 grdDim = {(uint) screen.half_width, (uint) screen.half_height, 1};
    dim3 blkDim = {2, 2, 1};
    _clear_depth_buffer << < grdDim, blkDim >> > (depth_buffer, screen);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
}

void fast_fill_depth_buffer(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                            const VirtualScreen screen) {
    dim3 grdDim = {(uint) count, 1, 1};
    dim3 blkDim = {4, 4, 1};
    _fast_render_triangles << < grdDim, blkDim >> > (triangles, count, depth_buffer, dst_image, true, screen);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
}

void fast_render_triangles(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                           const VirtualScreen screen) {

    dim3 grdDim = {(uint) count, 1, 1};
    dim3 blkDim = {4, 4, 1};
    _fast_render_triangles << < grdDim, blkDim >> > (triangles, count, depth_buffer, dst_image, false, screen);
    cudaDeviceSynchronize();
    checkCudaErrors(cudaGetLastError());
}