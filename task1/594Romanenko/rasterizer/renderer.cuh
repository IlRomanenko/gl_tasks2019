#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>


void clear_depth_buffer(int *depth_buffer, const VirtualScreen screen);

void render_triangles(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                      const VirtualScreen screen);

void fast_render_triangles(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                           const VirtualScreen screen);

void fast_fill_depth_buffer(const Triangle *triangles, size_t count, int *depth_buffer, glm::u8vec4 *dst_image,
                            const VirtualScreen screen);