#include <cudasoil/cuda_soil.h>
#include <common/CudaImage.h>
#include <common/CudaTimeMeasurement.h>
#include <common/CudaInfo.h>

#include <vector>

#include "renderer.cuh"

std::vector<Triangle> createSphere(glm::fvec3 center, float radius, uint circle_count, uint points_in_circle) {
    std::vector<Triangle> result;

    double y_step = M_PI / (circle_count + 2);
    double x_step = 2 * M_PI / points_in_circle;

    std::vector<std::vector<glm::fvec3>> points;
    std::vector<std::vector<glm::fvec3>> colors;

    points.push_back({center + glm::fvec3(0, 0, radius)});
    colors.push_back({{0, 0, 1}});

    for (uint i = 1; i < circle_count + 1; i++) {
        points.emplace_back();
        colors.emplace_back();
        double z = cos(y_step * i) * radius;
        double f = sin(y_step * i);
        double delta = x_step / 2 * (i % 2);
        for (uint j = 0; j <= points_in_circle + 1; j++) {
            double x = f * cos(x_step * j + delta) * radius;
            double y = f * sin(x_step * j + delta) * radius;
            points.back().push_back(center + glm::fvec3(x, y, z));
            colors.back().push_back(glm::fvec3(x, y, z) / (2 * radius) + glm::fvec3(0.5));
        };
    }
    points.push_back({center + glm::fvec3(0, 0, -radius)});
    colors.push_back({{0, 0, 0}});

    for (uint i = 1; i <= points_in_circle; i++) {
        result.emplace_back(points[0][0], points[1][i - 1], points[1][i], colors[0][0], colors[1][i - 1], colors[1][i]);
    }
    for (uint i = 2; i < circle_count + 1; i++) {
        for (uint j = 0; j < points_in_circle; j++) {
            result.emplace_back(points[i - 1][j], points[i - 1][j + 1], points[i][j],
                              colors[i - 1][j], colors[i - 1][j + 1], colors[i][j]);

        }
        for (uint j = 1; j <= points_in_circle; j++) {
            result.emplace_back(points[i][j - 1], points[i][j], points[i - 1][j],
                              colors[i][j - 1], colors[i][j], colors[i - 1][j]);

        }
    }

    for (uint i = 1; i <= points_in_circle; i++) {
        result.emplace_back(points.back()[0], points[circle_count][i - 1], points[circle_count][i],
                colors.back()[0], colors[circle_count][i - 1], colors[circle_count][i]);
    }
    return result;
}


void fast_check_sphere() {
    const int IMAGE_WIDTH = 1024;
    const int IMAGE_HEIGHT = 768;

    auto dstImage = std::make_shared<CudaImage4c>(IMAGE_WIDTH, IMAGE_HEIGHT, ResourceLocation::Device);
    VirtualScreen screen(-25, IMAGE_WIDTH / 2, IMAGE_HEIGHT / 2, 6.0, 6.0);

    std::vector<Triangle> spheres = createSphere({900, 0, 500}, 200, 1000, 1000);
    std::vector<Triangle> sphere2 = createSphere({0, 0, 10}, 10, 1000, 1000);
    for (const auto elem : sphere2) {
        spheres.push_back(elem);
    }
    auto triangles = spheres.data();
    size_t count = spheres.size();

    std::cout << "Total number of triangles : " << count << std::endl;

    auto deviceTriangles = std::make_shared<CudaResource<Triangle>>(count * sizeof(Triangle), ResourceLocation::Device);
    cudaMemcpy(deviceTriangles->getHandle(), triangles, deviceTriangles->getSize(), cudaMemcpyHostToDevice);
    auto depthBuffer = std::make_shared<CudaResource<int>>(IMAGE_HEIGHT * IMAGE_WIDTH * sizeof(int), ResourceLocation::Device);

    cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());

    CudaTimeMeasurement timer;
    timer.start();
    clear_depth_buffer(depthBuffer->getHandle(), screen);
    fast_fill_depth_buffer(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    fast_render_triangles(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    timer.stop();
    std::cout << "Fast render : " << timer.getSeconds() << " seconds" << std::endl;

    save_image_rgb("594RomanenkoData1/super_spheres_fast.png", dstImage);
}

void check_small_spheres() {
    const int IMAGE_WIDTH = 1024;
    const int IMAGE_HEIGHT = 768;

    auto dstImage = std::make_shared<CudaImage4c>(IMAGE_WIDTH, IMAGE_HEIGHT, ResourceLocation::Device);
    VirtualScreen screen(-25, IMAGE_WIDTH / 2, IMAGE_HEIGHT / 2, 6.0, 6.0);

    std::vector<Triangle> spheres = createSphere({900, 0, 500}, 200, 10, 10);
    std::vector<Triangle> sphere2 = createSphere({0, 0, 10}, 10, 10, 10);
    for (const auto elem : sphere2) {
        spheres.push_back(elem);
    }
    auto triangles = spheres.data();
    size_t count = spheres.size();

    std::cout << "Total number of triangles : " << count << std::endl;

    auto deviceTriangles = std::make_shared<CudaResource<Triangle>>(count * sizeof(Triangle), ResourceLocation::Device);
    cudaMemcpy(deviceTriangles->getHandle(), triangles, deviceTriangles->getSize(), cudaMemcpyHostToDevice);
    auto depthBuffer = std::make_shared<CudaResource<int>>(IMAGE_HEIGHT * IMAGE_WIDTH * sizeof(int), ResourceLocation::Device);


    CudaTimeMeasurement timer;

    cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());
    clear_depth_buffer(depthBuffer->getHandle(), screen);
    timer.start();
    render_triangles(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    timer.stop();

    std::cout << "Slow render : " << timer.getSeconds() << " seconds" << std::endl;

    save_image_rgb("594RomanenkoData1/spheres_slow.png", dstImage);

    cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());
    clear_depth_buffer(depthBuffer->getHandle(), screen);
    timer.start();
    fast_fill_depth_buffer(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    fast_render_triangles(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    timer.stop();
    std::cout << "Fast render : " << timer.getSeconds() << " seconds" << std::endl;
    save_image_rgb("594RomanenkoData1/spheres_fast.png", dstImage);
}

void check_triangles() {
    const int IMAGE_WIDTH = 1024;
    const int IMAGE_HEIGHT = 768;

    auto dstImage = std::make_shared<CudaImage4c>(IMAGE_WIDTH, IMAGE_HEIGHT, ResourceLocation::Device);
    VirtualScreen screen(-25, IMAGE_WIDTH / 2, IMAGE_HEIGHT / 2, 6.0, 6.0);

    glm::fvec4 red = {1, 0, 0, 1};
    glm::fvec4 green = {0, 1, 0, 1};
    glm::fvec4 blue = {0, 0, 1, 1};
    glm::fvec4 white = {1, 1, 1, 1};
    glm::fvec4 black = {0, 0, 0, 1};

    Triangle triangles[] = {
            {{-300, 0, 0}, {-100, 100, -5}, {100, 100, 500}, red, green, blue},

            {{50, 50, 10}, {200, 400, 200}, {400, 200, 200}, red, red, red},
            {{0, 0, 20}, {0, 150, 20}, {150, 0, 20}, white, white, white},
    };
    size_t count = sizeof(triangles) / sizeof(Triangle);


    auto deviceTriangles = std::make_shared<CudaResource<Triangle>>(count * sizeof(Triangle), ResourceLocation::Device);
    cudaMemcpy(deviceTriangles->getHandle(), triangles, deviceTriangles->getSize(), cudaMemcpyHostToDevice);

    auto depthBuffer = std::make_shared<CudaResource<int>>(IMAGE_HEIGHT * IMAGE_WIDTH * sizeof(int), ResourceLocation::Device);

    cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());
    clear_depth_buffer(depthBuffer->getHandle(), screen);
    render_triangles(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    save_image_rgb("594RomanenkoData1/triangles_slow.png", dstImage);

    cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());

    clear_depth_buffer(depthBuffer->getHandle(), screen);
    fast_fill_depth_buffer(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    fast_render_triangles(deviceTriangles->getHandle(), count, depthBuffer->getHandle(), dstImage->getHandle(), screen);
    save_image_rgb("594RomanenkoData1/triangles_fast.png", dstImage);
}

int main() {
    printCudaInfo();

    check_triangles();
    check_small_spheres();
    fast_check_sphere();

    return 0;
}
