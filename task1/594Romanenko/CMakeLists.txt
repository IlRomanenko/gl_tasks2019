file(GLOB_RECURSE SRC_FILES
        ${CMAKE_CURRENT_SOURCE_DIR}
        *.cpp *.h *.cu *.cuh
)

message("Task 1 sources: ${SRC_FILES}")

MAKE_CUDA_TASK(594Romanenko 1 "${SRC_FILES}")

target_include_directories(594Romanenko1 PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
