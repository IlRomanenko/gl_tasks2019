#ifndef COMMON_FUNCTIONS_CUH
#define COMMON_FUNCTIONS_CUH

#include <glm/glm.hpp>

struct VirtualScreen {
    glm::float32 observer;

    glm::float32 width_scale;
    glm::float32 height_scale;

    glm::int32 half_width;
    glm::int32 half_height;

    VirtualScreen(glm::float32 _observer, glm::int32 _half_width, glm::int32 _half_height,
                      glm::float32 width_scale, glm::float32 height_scale)
            : height_scale(height_scale), width_scale(width_scale) {
        observer = _observer;
        half_width = _half_width;
        half_height = _half_height;
    }
};

struct Triangle {
    glm::fvec3 a, b, c;
    glm::fvec4 color_a, color_b, color_c;

    Triangle(glm::fvec3 _a, glm::fvec3 _b, glm::fvec3 _c,
            glm::fvec4 _color_a, glm::fvec4 _color_b, glm::fvec4 _color_c) {
        a = _a;
        b = _b;
        c = _c;
        color_a = _color_a;
        color_b = _color_b;
        color_c = _color_c;
    }

    Triangle(glm::fvec3 _a, glm::fvec3 _b, glm::fvec3 _c,
             glm::fvec3 _color_a, glm::fvec3 _color_b, glm::fvec3 _color_c) {
        a = _a;
        b = _b;
        c = _c;
        color_a = glm::fvec4(_color_a.x, _color_a.y, _color_a.z, 1);
        color_b = glm::fvec4(_color_b.x, _color_b.y, _color_b.z, 1);
        color_c = glm::fvec4(_color_c.x, _color_c.y, _color_c.z, 1);
    }
};

#endif // COMMON_FUNCTIONS_CUH
